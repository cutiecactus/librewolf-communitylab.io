---
title: Signing keys
---

PGP key fingerprints of [LibreWolf maintainers](https://gitlab.com/groups/librewolf-community/-/group_members).

| GitLab username | Key fingerprint |
| --------------- | --------------- |
| [@shreyasminocha](https://gitlab.com/shreyasminocha) | `9D70 08F8 DFCD 2150 8174 954A 3740 FE9F C577 2203` |
| [@ohfp](https://gitlab.com/ohfp)           | `031F 7104 E932 F7BD 7416 E7F6 D284 5E13 05D6 E801` |
